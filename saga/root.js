import { fork, take, put, select } from 'redux-saga/effects'
import * as storeActions from '../store/action'
import * as sagaActions from '../saga/action'
import { url } from '../config'

let apiCaller = null

function* watchAddAppointment() {
    while (true) {
        const action = yield take(sagaActions.APPOINTMENT_ADD)
        const appointment = yield fetch(`${url}/appointment`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
            },
            body: JSON.stringify(action.payload),
        }).then(d => d.json())
        yield put(storeActions.appointmentReceive([appointment]))
    }
}

function* watchUpdateAppointment() {
    while (true) {
        const action = yield take(sagaActions.APPOINTMENT_FETCH_BY_ID)
        const appointment = yield fetch(`${url}/appointment/${action.payload.id}`).then(d => d.json())

        yield put(storeActions.appointmentReplace(appointment))
    }
}

function* watchFetchAppointments() {
    while (true) {
        const action = yield take(sagaActions.APPOINTMENT_FETCH)
        const appointments = yield fetch(`${url}/appointment/?shopId=${action.payload.shopId}`).then(d => d.json())
        yield put(storeActions.appointmentReceive(appointments))
    }
}

function* watchFetchShops() {
    while (true) {
        yield take(sagaActions.SHOPS_FETCH)
        const shops = yield fetch(`${url}/shop`).then(d => d.json())
        yield put(storeActions.shopReceive(shops))
    }
}

function* watchEveriPassGenerate() {
    while (true) {
        const { payload: appointment } = yield take(sagaActions.EVERIPASS_GENERATE)
        const { privkey, pubkey } = yield select(({ user }) => user)

        const res = yield fetch(`${url}/everipass/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
            },
            body: JSON.stringify({ appointment, pubkey, privkey }),
        }).then(d => d.json())

        yield put(storeActions.appointmentReplace({ ...appointment, qr: res.dataUrl }))
    }
}

function* watchScanEveriPass() {
    while (true) {
        const { payload: { id, data } } = yield take(sagaActions.EVERIPASS_SCAN)
        const { privkey } = yield select(({ user }) => user)
        const updateAppointment = yield fetch(`${url}/everipass/verify`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
            },
            body: JSON.stringify({ link: data, id, privkey }),
        }).then(d => d.json())

        yield put(storeActions.appointmentReplace(updateAppointment))
    }
}

function* watchAppointmentConfirm() {
    while (true) {
        const { payload: appointment } = yield take(sagaActions.APPOINTMENT_CONFIRM)
        const { privkey, pubkey } = yield select(({ user }) => user)

        const updateAppointment = yield fetch(`${url}/appointment/${appointment.id}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
            },
            body: JSON.stringify({ appointment, pubkey, privkey }),
        }).then(d => d.json())

        yield put(storeActions.appointmentReplace(updateAppointment))
    }
}

export default function* root() {
    yield fork(watchFetchShops)
    yield fork(watchAddAppointment)
    yield fork(watchUpdateAppointment)
    yield fork(watchFetchAppointments)
    yield fork(watchAppointmentConfirm)
    yield fork(watchEveriPassGenerate)
    yield fork(watchScanEveriPass)
}
