export const SHOPS_FETCH = 'SHOPS_FETCH'
export const APPOINTMENT_ADD = 'APPOINTMENT_ADD'
export const APPOINTMENT_FETCH_BY_ID = 'APPOINTMENT_FETCH_BY_ID'
export const APPOINTMENT_FETCH = 'APPOINTMENT_FETCH'
export const APPOINTMENT_CONFIRM = 'APPOINTMENT_CONFIRM'
export const EVERIPASS_GENERATE = 'EVERIPASS_GENERATE'
export const EVERIPASS_SCAN = 'EVERIPASS_SCAN'

export const fetchShops = () => ({
    type: SHOPS_FETCH,
})

export const addAppointment = (shopId, pubkey) => ({
    type: APPOINTMENT_ADD,
    payload: { shopId, pubkey, time: Date.now() },
})

export const fetchAppointmentById = id => ({
    type: APPOINTMENT_FETCH_BY_ID,
    payload: { id },
})

export const fetchAppointment = shopId => ({
    type: APPOINTMENT_FETCH,
    payload: { shopId },
})

export const confirmAppointment = payload => ({
    type: APPOINTMENT_CONFIRM,
    payload,
})

export const generateEveriPass = payload => ({
    type: EVERIPASS_GENERATE,
    payload,
})

export const scanPass = (id, data) => ({
    type: EVERIPASS_SCAN,
    payload: { id, data },
})
