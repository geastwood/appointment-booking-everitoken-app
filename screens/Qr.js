import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { BarCodeScanner, Permissions } from 'expo'
import * as uiActions from '../saga/action'
import { connect } from 'react-redux'
import { debounce } from 'lodash'

class BarcodeScannerComponent extends React.Component {
    state = {
        hasCameraPermission: null,
    }

    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA)
        this.setState({ hasCameraPermission: status === 'granted' })
    }

    render() {
        const { hasCameraPermission } = this.state

        if (hasCameraPermission === null) {
            return null
        }
        if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>
        }
        return (
            <View style={{ flex: 1 }}>
                <BarCodeScanner onBarCodeScanned={this.handleBarCodeScanned} style={StyleSheet.absoluteFill} />
            </View>
        )
    }

    handleBarCodeScanned = debounce(
        ({ data }) => {
            this.props.onScan(this.props.id, data)
        },
        3000,
        { leading: true },
    )
}

export default (ConnectedBarcodeScanner = connect(
    ({ user }, { navigation: { getParam } }) => {
        return { user, id: getParam('id') }
    },
    {
        onScan: (id, data) => uiActions.scanPass(id, data),
    },
)(BarcodeScannerComponent))
