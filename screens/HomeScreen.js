import React from 'react'
import { createStackNavigator } from 'react-navigation'
import { ScrollView, Text, View, Image, Platform } from 'react-native'
import { connect } from 'react-redux'
import ShopList from './ShopList'
import ShopDetail from './ShopDetail'
import Dashboard from './ShopDashboard'
import Detail from './AppointmentDetail'
import { RkText, RkButton } from 'react-native-ui-kitten'
import * as storeActions from '../store/action'
import Qr from './Qr'

const RoleSelection = ({ user, onPress }) => (
    <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
        <View style={{ alignSelf: 'center' }}>
            <Image resizeMode="cover" source={require('../assets/images/icon.png')} />
        </View>
        <View
            style={[
                { alignSelf: 'center' },
                Platform.select({
                    ios: { paddingBottom: 100, paddingTop: 30 },
                    android: { paddingBottom: 50, paddingTop: 10 },
                }),
            ]}
        >
            <RkText style={{ fontSize: 24 }}>Select your role to proceed</RkText>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <RkButton onPress={() => onPress('user')}>
                <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>User</Text>
            </RkButton>
            <RkButton onPress={() => onPress('shop')}>
                <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Shop</Text>
            </RkButton>
        </View>
    </ScrollView>
)

const ConnectedRoleSeletion = connect(null, {
    onPress: role => storeActions.userReceive(role),
})(RoleSelection)

const MainScreen = ({ user, ...props }) => {
    if (user.role === null) {
        return <ConnectedRoleSeletion user={user} />
    }

    return user.role === 'user' ? <ShopList {...props} /> : <Dashboard {...props} />
}

const ConncetedMainScreen = connect(({ user }) => ({ user }))(MainScreen)

const MainNavigator = createStackNavigator({
    Main: {
        screen: ConncetedMainScreen,
        navigationOptions: () => ({
            title: 'Booking with EveriToken',
        }),
    },
    Qr: {
        screen: Qr,
    },
    ShopDetail: {
        screen: ShopDetail,
        navigationOptions: ({ navigation }) => ({
            title: navigation.getParam('title'),
        }),
    },
})

const RootNavigator = createStackNavigator(
    {
        Main: {
            screen: MainNavigator,
        },
        Detail: {
            screen: Detail,
        },
    },
    { mode: 'modal', headerMode: 'none' },
)

export default RootNavigator
