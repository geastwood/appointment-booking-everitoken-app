import React from 'react'
import { createStackNavigator } from 'react-navigation'
import { sortBy } from 'lodash'
import { ScrollView, View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import * as uiActions from '../saga/action'

import ShopTeaser from '../components/ShopTeaser'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
})

class ListScreen extends React.Component {
    static navigationOptions = {
        header: null,
    }

    componentWillMount() {
        this.props.onMount()
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                    {this.props.shops.map(shop => (
                        <ShopTeaser
                            key={shop.id}
                            shop={shop}
                            onPress={() =>
                                this.props.navigation.navigate('ShopDetail', { id: shop.id, title: shop.name })
                            }
                        />
                    ))}
                </ScrollView>
            </View>
        )
    }
}

const ConnectedListScreen = connect(
    ({ shops, user }) => ({
        shops: sortBy(shops, 'id'),
        user,
    }),
    {
        onMount: uiActions.fetchShops,
    },
)(ListScreen)

export default ConnectedListScreen
