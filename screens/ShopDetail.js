import React from 'react'
import { createStackNavigator } from 'react-navigation'
import { Text, View, StyleSheet, FlatList } from 'react-native'
import { connect } from 'react-redux'
import ShopTeaser from '../components/ShopTeaser'
import AppointmentTeaser from '../components/AppointmentTeaser'
import { RkButton } from 'react-native-ui-kitten'
import { sortBy } from 'lodash'
import * as uiActions from '../saga/action'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    contentContainer: {
        paddingTop: 30,
    },
})
class DetailScreen extends React.Component {
    componentWillMount() {
        this.props.onMount(this.props.shop.id)
    }
    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    ListHeaderComponent={() => <ShopTeaser compact shop={this.props.shop} />}
                    data={this.props.appointments}
                    keyExtractor={({ id }) => String(id)}
                    renderItem={({ item }) => (
                        <View style={{ paddingHorizontal: 16, paddingVertical: 8 }}>
                            <AppointmentTeaser
                                appointment={item}
                                onDetailPress={() =>
                                    this.props.navigation.navigate('Detail', {
                                        id: this.props.shop.id,
                                        appointmentId: item.id,
                                    })
                                }
                                onRefreshPress={id => this.props.onFetchAppointmentById(id)}
                            />
                        </View>
                    )}
                    ListEmptyComponent={() => (
                        <View style={{ paddingTop: '30%', alignItems: 'center' }}>
                            <Text style={{ padding: 20 }}>There is currently no appointment.</Text>
                            <RkButton
                                style={{ width: '50%' }}
                                rkType="outline"
                                onPress={() => this.props.onAddAppointment(this.props.shop.id, this.props.user.pubkey)}
                            >
                                Make appointment
                            </RkButton>
                        </View>
                    )}
                />
            </View>
        )
    }
}

const ConnectedDetailScreen = connect(
    ({ shops, appointments, user }, { navigation: { getParam } }) => ({
        shop: shops.find(shop => shop.id === getParam('id')),
        appointments: sortBy(appointments, 'time'),
        user,
    }),
    {
        onMount: uiActions.fetchAppointment,
        onAddAppointment: uiActions.addAppointment,
        onFetchAppointmentById: uiActions.fetchAppointmentById,
    },
)(DetailScreen)

export default ConnectedDetailScreen
