import React from 'react'
import { ScrollView, Image, Text, View, Dimensions } from 'react-native'
import { connect } from 'react-redux'
import { MonoText } from '../components/StyledText'
import moment from 'moment'
import { RkButton } from 'react-native-ui-kitten'
import * as uiActions from '../saga/action'
import { get } from 'lodash'

class DetailScreen extends React.Component {
    render() {
        const { appointment, onGenerateEveriPass, onRefreshPress } = this.props
        const { width } = Dimensions.get('window')
        const tokenDestroyed = get(appointment, 'token.destroyed', false)
        return (
            <ScrollView style={{ paddingHorizontal: 16, paddingVertical: 8 }}>
                <Text style={{ textAlign: 'center', fontSize: 24, paddingVertical: 20 }}>Appointment Detail</Text>
                <MonoText>Id: {appointment.id}</MonoText>
                <MonoText>Date: {moment(appointment.time).format('MMMM Do YYYY, h:mm:ss a')}</MonoText>
                <MonoText>State: {appointment.confirmed ? 'confirmed' : 'pending'}</MonoText>
                <MonoText>Destroyed: {tokenDestroyed ? 'Yes' : 'Not yet'}</MonoText>
                {tokenDestroyed && <MonoText>Destroy Tx: {appointment.token.destroyTx}</MonoText>}
                <MonoText>User: {appointment.pubkey}</MonoText>
                {appointment.confirmed && <MonoText>Token name: {appointment.token.name}</MonoText>}
                {appointment.confirmed && <MonoText>Token Trans. Tx: {appointment.token.transferTx}</MonoText>}
                {appointment.confirmed && <MonoText>Token Trans. Tx: {appointment.token.transferTx}</MonoText>}
                {appointment.qr && (
                    <View style={{ alignSelf: 'center' }}>
                        <Image source={{ uri: appointment.qr }} style={{ width, height: width }} resizeMode="contain" />
                    </View>
                )}

                <View style={{ paddingTop: 8 }}>
                    <RkButton
                        onPress={() => {
                            onRefreshPress(appointment.id)
                        }}
                        rkType="outline"
                        style={{ width: '100%', paddingTop: 8 }}
                    >
                        Refresh
                    </RkButton>
                </View>
                <View style={{ paddingTop: 8 }}>
                    <RkButton
                        onPress={() => {
                            if (tokenDestroyed) {
                                alert('Token is already destroyed')
                                return
                            }
                            if (!appointment.confirmed) {
                                alert("Not yet confirmed, can't generate EveriPass")
                                return
                            }
                            onGenerateEveriPass(appointment)
                        }}
                        rkType="outline"
                        style={{ width: '100%', paddingTop: 8 }}
                    >
                        Generate EveriPass
                    </RkButton>
                </View>
            </ScrollView>
        )
    }
}

const ConnectedDetailScreen = connect(
    ({ user, appointments }, { navigation: { getParam } }) => {
        const id = getParam('id')
        const appointmentId = getParam('appointmentId')
        return {
            id,
            appointment: appointments.find(a => a.id === appointmentId),
            user,
        }
    },
    {
        onGenerateEveriPass: uiActions.generateEveriPass,
        onRefreshPress: uiActions.fetchAppointmentById,
    },
)(DetailScreen)

export default ConnectedDetailScreen
