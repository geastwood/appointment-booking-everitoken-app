import * as React from 'react'
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { sortBy, get } from 'lodash'
import * as uiActions from '../saga/action'
import { RkCard, RkText, RkButton, RkTheme } from 'react-native-ui-kitten'
import moment from 'moment'
import { MonoText } from '../components/StyledText'

class Dashboard extends React.PureComponent {
    componentWillMount() {
        this.props.onMount(this.props.user.shopId)
    }
    state = {
        selectedItem: null,
    }
    handleSelection = selectedItem => {
        this.setState({ selectedItem })
    }
    render() {
        const { user, appointments } = this.props
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    padding: 16,
                }}
            >
                <Text
                    style={{
                        color: '#424242',
                        alignSelf: 'center',
                        fontSize: 24,
                        fontWeight: 'bold',
                        paddingBottom: 20,
                    }}
                >
                    Dashboard for {user.name} (ID: {user.shopId})
                </Text>
                <FlatList
                    ListHeaderComponent={() => (
                        <Text style={{ textAlign: 'right' }}>There are currently {appointments.length} entries</Text>
                    )}
                    data={appointments}
                    keyExtractor={({ id }) => String(id)}
                    renderItem={({ item }) => {
                        const tokenDestroyed = get(item, 'token.destroyed', false)
                        return (
                            <View style={{ paddingHorizontal: 16, paddingVertical: 8 }}>
                                <TouchableOpacity onPress={() => this.handleSelection(item)}>
                                    <MonoText>Id: {item.id}</MonoText>
                                    <MonoText>Date: {moment(item.time).format('MMMM Do YYYY, h:mm:ss a')}</MonoText>
                                    <MonoText>State: {item.confirmed ? 'confirmed' : 'pending'}</MonoText>
                                    <MonoText>Destroyed: {tokenDestroyed ? 'Yes' : 'Not yet'}</MonoText>
                                    {tokenDestroyed && <MonoText>Destroy Tx: {item.token.destroyTx}</MonoText>}
                                    <MonoText>User: {item.pubkey}</MonoText>
                                    {item.confirmed && <MonoText>Token name: {item.token.name}</MonoText>}
                                    {item.confirmed && <MonoText>Token Trans. Tx: {item.token.transferTx}</MonoText>}
                                </TouchableOpacity>
                            </View>
                        )
                    }}
                    ListEmptyComponent={() => (
                        <View style={{ paddingTop: '30%', alignItems: 'center' }}>
                            <Text style={{ padding: 20 }}>There is currently no appointment.</Text>
                        </View>
                    )}
                />
                <View
                    style={{
                        height: '35%',
                        paddingTop: 10,
                        borderTopColor: '#ccc',
                        borderTopWidth: StyleSheet.hairlineWidth,
                    }}
                >
                    {this.state.selectedItem ? (
                        <View>
                            <Text>Current selection is:</Text>
                            <MonoText style={{ fontWeight: 'bold' }}>{this.state.selectedItem.id}</MonoText>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 20 }}>
                                <RkButton
                                    onPress={() => this.props.onConfirm(this.state.selectedItem)}
                                    rkType="outline"
                                    style={{ width: '40%' }}
                                >
                                    Confirm
                                </RkButton>
                                <RkButton
                                    onPress={() =>
                                        this.props.navigation.navigate('Qr', { id: this.state.selectedItem.id })
                                    }
                                    rkType="outline"
                                    style={{ width: '40%' }}
                                >
                                    Scan to checkin
                                </RkButton>
                            </View>
                        </View>
                    ) : (
                        <Text>Select an Appointment to continue</Text>
                    )}
                </View>
                <RkButton onPress={() => this.props.onMount(user.shopId)} rkType="outline" style={{ width: '100%' }}>
                    Refresh all appointments
                </RkButton>
            </View>
        )
    }
}

export default connect(
    ({ user, appointments }) => ({
        user,
        appointments: sortBy(appointments.filter(d => d.shopId === user.shopId), 'time'),
    }),
    {
        onMount: id => uiActions.fetchAppointment(id),
        onConfirm: uiActions.confirmAppointment,
    },
)(Dashboard)
