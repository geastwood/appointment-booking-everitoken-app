import * as storeAction from '../action'

export const defaultState = []

export default (state = defaultState, action) => {
    switch (action.type) {
        case storeAction.SHOP_RECEIVE:
            return [...state, ...action.payload]
        case storeAction.SHOP_UPDATE:
            return [...state.filter(({ id }) => id !== action.payload.id), action.payload]
        default:
            return state
    }
}
