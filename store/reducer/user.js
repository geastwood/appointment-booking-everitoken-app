import * as storeActions from '../action'
const defaultState = {
    name: '',
    role: null,
    shopId: null,
    pubkey: '',
    privkey: '',
}

export default (state = defaultState, action) => {
    if (action.type === storeActions.USER_RECEIVE) {
        if (action.payload === 'user') {
            return {
                name: 'Fei',
                role: action.payload,
                privkey: '5KWPncxivQBtxhQ3ToijyAbUJMLJp6RdtEhKB2z5oHiDfC6kwQA',
                pubkey: 'EVT4w391Nin1yu6q5GUAPYdjSLKwnES4DWvGr1oe3M7Jga18cPwiA',
            }
        } else {
            return {
                name: 'Shop A',
                shopId: 1,
                role: action.payload,
                privkey: '5JkQtbsHsTEdmBGd9Zmniyr1C7q3fNEzcXSM35a3J8Cs5NT5BYB',
                pubkey: 'EVT8it3oPTKRMX8LHSkJdZUspfaUhpKLBJPx7wBx45ifwfy1VwPxW',
            }
        }
    }
    return state
}
