import * as storeAction from '../action'
import { uniqBy } from 'lodash'

const testData = []

export const defaultState = [...testData]

export default (state = defaultState, action) => {
    switch (action.type) {
        case storeAction.APPOINTMENT_RECEIVE: {
            return uniqBy([...state, ...action.payload], 'id')
        }
        case storeAction.APPOINTMENT_REPLACE:
            return [...state.filter(d => d.id !== action.payload.id), action.payload]
        default:
            return state
    }
}
