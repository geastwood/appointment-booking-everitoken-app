import { combineReducers } from 'redux'
import shops from './shops'
import appointments from './appointments'
import user from './user'

export default combineReducers({
    shops,
    appointments,
    user,
})
