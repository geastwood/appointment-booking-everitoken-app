export const SHOP_RECEIVE = 'SHOP_REREIVE'
export const SHOP_UPDATE = 'SHOP_UPDATE'
export const APPOINTMENT_RECEIVE = 'APPOINTMENT_RECEIVE'
export const APPOINTMENT_REPLACE = 'APPOINTMENT_REPLACE'
export const USER_RECEIVE = 'USER_RECEIVE'

export const shopReceive = shops => ({
    type: SHOP_RECEIVE,
    payload: shops,
})

export const shopUpdate = shop => ({
    type: SHOP_UPDATE,
    payload: shop,
})

export const appointmentReceive = appointments => ({
    type: APPOINTMENT_RECEIVE,
    payload: appointments,
})

export const appointmentReplace = appointments => ({
    type: APPOINTMENT_REPLACE,
    payload: appointments,
})

export const userReceive = payload => ({
    type: USER_RECEIVE,
    payload,
})
