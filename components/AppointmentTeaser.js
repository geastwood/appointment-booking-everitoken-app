import * as React from 'react'
import { View, Image, Text, Button, StyleSheet } from 'react-native'
import { RkCard, RkText, RkButton, RkTheme } from 'react-native-ui-kitten'
import { MonoText } from '../components/StyledText'
import moment from 'moment'

const styles = {
    container: {
        padding: 8,
    },
    description: {
        fontSize: 18,
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        paddingRight: 10,
    },
}

const getText = appointment =>
    `${moment(appointment.time).format('MMMM Do YYYY, h:mm:ss a')} (${appointment.confirmed ? 'confirmed' : 'pending'})`

const AppointmentTeaser = ({ appointment, onDetailPress, onRefreshPress }) => (
    <View rkType="story" style={{ padding: 10 }}>
        <MonoText>Time: {moment(appointment.time).format('MMMM Do YYYY, h:mm:ss a')}</MonoText>
        <MonoText>Status: {appointment.confirmed ? 'confirmed' : 'pending'}</MonoText>

        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 10 }}>
            <View style={{ paddingRight: 10 }}>
                <Button title="Refresh" onPress={() => onRefreshPress(appointment.id)} />
            </View>
            <Button title="Detail" onPress={onDetailPress} />
        </View>
    </View>
)
export default AppointmentTeaser
