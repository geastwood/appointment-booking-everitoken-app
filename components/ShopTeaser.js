import * as React from 'react'
import { View, Image, Text } from 'react-native'
import { RkCard, RkText, RkButton, RkTheme } from 'react-native-ui-kitten'

const styles = {
    container: {
        padding: 8,
    },
    description: {
        fontSize: 18,
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        paddingRight: 10,
    },
}

const ShopTeaser = ({ shop, onPress, compact = false }) => (
    <RkCard rkType="story">
        <Image rkCardImg source={{ uri: shop.image }} resizeMode="cover" style={{ height: 180 }} />
        <View style={[styles.container, { flexDirection: 'row', alignItems: 'center' }]}>
            <Text style={styles.header}>{shop.name}</Text>
            <RkButton rkType="small circle success">{shop.category}</RkButton>
        </View>

        {!compact && (
            <View style={styles.container}>
                <Text numberOfLines={3} style={styles.description}>
                    {shop.description}
                </Text>
            </View>
        )}

        {!compact && (
            <View rkCardFooter style={{ alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'flex-end' }}>
                <RkButton rkType="outline" onPress={onPress}>
                    Book
                </RkButton>
            </View>
        )}
    </RkCard>
)

export default ShopTeaser
